<!-- ./php/index.php -->
<?php

$mysqli = new mysqli("192.168.1.39:9906","devuser","devpass","test_db");

// Check connection
if ($mysqli -> connect_errno) {
  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
  exit();
}
 
$sql = 'SELECT * FROM tbproduct';
$result = $mysqli ->query($sql);

$table = '<table border=1><tr><td>No.</td><td>Name</td><td>Code</td><td>Price</td></tr>';
while ($row = $result->fetch_assoc()) {
    $id = $row['id'];
    $name = $row['name'];
    $code = $row['code'];
    $price = $row['price'];
    $table .= "<tr><td>$id</td><td>$name</td><td>$code</td><td>$price</td></tr>";
}
$table .= '</table>';

?>

<html>
    <head>
        <title>Hello World</title>
    </head>

    <body>
        <?php
            echo "Hello, World!<br>";
            echo $table;
        ?>
    </body>
</html>