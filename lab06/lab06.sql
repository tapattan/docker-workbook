-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 10, 2022 at 01:35 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `dbTrain`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbmember`
--

CREATE TABLE `tbmember` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `mobile` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbmember`
--

INSERT INTO `tbmember` (`id`, `name`, `email`, `age`, `mobile`) VALUES
(1, 'lisa', 'lisa2022@gmail.com', 18, '0888888888'),
(2, 'george', 'george@gmail.com', 20, '0999999999'),
(13, 'ronaldo', 'cr7@gmail.com', 23, 'n/a'),
(14, 'Vardy', 'vd9@gmail.com', 24, 'n/a');

-- --------------------------------------------------------

--
-- Table structure for table `tbmonthlyreport`
--

CREATE TABLE `tbmonthlyreport` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbmonthlyreport`
--

INSERT INTO `tbmonthlyreport` (`id`, `name`, `price`, `date`) VALUES
(171, 'george', 80, '2022-02'),
(172, 'lisa', 70, '2022-02'),
(173, 'Vardy', 30, '2022-02'),
(174, 'ronaldo', 30, '2022-02'),
(175, 'lisa', 60, '2022-01'),
(176, 'george', 20, '2022-01'),
(177, 'ronaldo', 60, '2021-12'),
(178, 'Vardy', 60, '2021-12'),
(179, 'ronaldo', 80, '2021-11');

-- --------------------------------------------------------

--
-- Table structure for table `tbproduct`
--

CREATE TABLE `tbproduct` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `price` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbproduct`
--

INSERT INTO `tbproduct` (`id`, `name`, `code`, `price`) VALUES
(1, 'ปากกาน้ำเงิน', 'P01', '10'),
(2, 'สมุด', 'B01', '20'),
(3, 'ยางลบ', 'R01', '5'),
(4, 'ปากกาแดงหมึกแห้งไว', 'P02', '20'),
(49, 'ดินสอไม้', 'PW01', '10'),
(50, 'ดินสอไม้เปลี่ยนไส้ได้', 'PW02', '25');

-- --------------------------------------------------------

--
-- Table structure for table `tbtransaction`
--

CREATE TABLE `tbtransaction` (
  `id` int(11) NOT NULL,
  `memberid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbtransaction`
--

INSERT INTO `tbtransaction` (`id`, `memberid`, `productid`, `qty`, `date`) VALUES
(1, 1, 1, 2, '2022-02-03 23:11:06'),
(2, 1, 2, 1, '2022-02-03 23:11:06'),
(3, 1, 3, 1, '2022-02-03 23:11:33'),
(4, 2, 4, 2, '2022-02-03 23:11:33'),
(5, 1, 1, 2, '2022-02-07 21:55:41'),
(6, 2, 2, 2, '2022-02-07 21:56:31'),
(7, 1, 3, 1, '2022-02-07 21:56:31'),
(8, 13, 1, 2, '2022-02-10 13:47:26'),
(9, 14, 49, 3, '2022-02-10 13:47:26'),
(10, 13, 3, 2, '2022-02-10 13:47:46'),
(11, 14, 4, 2, '2021-12-23 00:00:00'),
(12, 13, 2, 4, '2021-11-17 00:00:00'),
(13, 13, 2, 3, '2021-12-15 00:00:00'),
(14, 14, 49, 2, '2021-12-20 00:00:00'),
(15, 2, 49, 2, '2022-01-11 00:00:00'),
(16, 1, 4, 3, '2022-01-23 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbmember`
--
ALTER TABLE `tbmember`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbmonthlyreport`
--
ALTER TABLE `tbmonthlyreport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbproduct`
--
ALTER TABLE `tbproduct`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbtransaction`
--
ALTER TABLE `tbtransaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1` (`memberid`),
  ADD KEY `FK2` (`productid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbmember`
--
ALTER TABLE `tbmember`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbmonthlyreport`
--
ALTER TABLE `tbmonthlyreport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT for table `tbproduct`
--
ALTER TABLE `tbproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `tbtransaction`
--
ALTER TABLE `tbtransaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbtransaction`
--
ALTER TABLE `tbtransaction`
  ADD CONSTRAINT `FK1` FOREIGN KEY (`memberid`) REFERENCES `tbmember` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2` FOREIGN KEY (`productid`) REFERENCES `tbproduct` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
